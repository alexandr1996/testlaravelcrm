require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vuex from 'vuex'
import user from './store/user'
import { routes } from './routes'

Vue.use(Vuex);
Vue.use(ElementUI)
Vue.use(VueRouter)

export const store = new Vuex.Store({
    modules: {
        user
    }
})

const router = new VueRouter({
    mode: 'history',
    routes
});

new Vue({
    el: '#app',
    router,
    store,
    render: app => app(App)
});
