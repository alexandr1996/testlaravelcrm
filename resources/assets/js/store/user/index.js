import axios from 'axios'

export default {
    namespaced: true,
    state: {
        token: localStorage.getItem('user-token') || '',
        status: '',
        test: 'lol'
    },
    actions: {
        login: ({commit}, user) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                axios({ url: 'api/login', data: user, method: 'POST' })
                    .then(resp => {
                        console.log(resp)
                        const token = resp.data.token
                        localStorage.setItem('user-token', token) // store the token in localstorage
                        commit("authSuccess", token)
                        commit("setUser", user)
                        // you have your token, now log in your user :)
                        resolve(resp)
                    })
                .catch(err => {
                    commit('authError')
                    localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
                    reject(err)
                })
            })
        },
        logout: ({ commit, dispatch }) => {
            return new Promise((resolve, reject) => {
                commit('authLogout')
                localStorage.removeItem('user-token') // clear your user's token from localstorage
                resolve()
            })
        }
    },
    mutations: {
        authLogout: () => {
            state.token = ''
        },
        userRequest: (state) => {
            state.status = 'loading'
        },
        authSuccess: (state, token) => {
            state.status = 'success'
            state.token = token
        },
        authError: (state) => {
            state.status = 'error'
        },
        setUser(state, user) {
            state.user = user
        }
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
    }
}